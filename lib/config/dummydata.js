'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Person = mongoose.model('Person'),
    Directory = mongoose.model('Directory'),
    Category = mongoose.model('Category');


/**
 * Populate database with sample application data
 */

// Clear old users, then add a default user
/*User.find({}).remove(function () {
  User.create({
        provider: 'local',
        name: 'User',
        username: 'BottinFestivals2014',
        password: 'BottinFestivals2014'
      }, {
        provider: 'local',
        name: 'Admin',
        username: 'BottinAdmin120691',
        password: 'BottinAdmin120691',
        role: 'admin'
      }, function () {
        console.log('finished populating users');
      }
  );
});*/

/*
// Add a person
Person.find({}).remove(function () {
  var person1 = new Person({
    name: 'Laurane',
    surname: 'Van Branteghem',
    jobTitle: 'Commis Cachets/Contrats',
    image: '',
    phone: 3478,
    phoneCell: 5142123478,
    email: '',
    note: '',
    category: '',
    directory: ''
  });

  var person2 = new Person({
    name: 'Ulysse',
    surname: 'Lemerise Bouchard',
    jobTitle: 'Photographe',
    image: '',
    phone: 4196,
    phoneCell: 5148044196,
    email: '',
    note: '',
    category: '',
    directory: ''
  });

  var person3 = new Person({
    name: 'Annie-Claude',
    surname: 'St-Gelais',
    jobTitle: 'Adjointe RP',
    image: '',
    phone: 5985,
    phoneCell: 5146881985,
    email: '',
    note: '',
    category: '',
    directory: ''
  });

  var person4 = new Person({
    name: 'Tom4',
    surname: 'Thierus',
    jobTitle: 'Animation',
    image: 'http://www.zoneski.com/logosapropos/thomast.jpg',
    phone: 8765,
    phoneCell: 5147658785,
    email: 'thomas.thiery@test.com',
    note: 'spécialiste en ski volant',
    category: '',
    directory: ''
  });

  person1.save();
  person2.save();
  person3.save();
  person4.save();

  Category.find({}).remove(function () {

    // Add a category
    var firstCategory = new Category({name: 'Administration'});
    var secondCategory = new Category({name: 'Photographes officiels'});
    var thirdCategory = new Category({name: 'Communications / Publicité'});

    firstCategory.persons.push(person1);
    secondCategory.persons.push(person2);
    secondCategory.persons.push(person4);
    thirdCategory.persons.push(person3);

    firstCategory.save();
    secondCategory.save();
    thirdCategory.save();

    Directory.find({}).remove(function () {

      Directory.create({
            name: 'Commercialisation et Finances',
            colorCode: '#e69138'
          }, {
            name: 'Présidence, MKG, Aff.Gouv, RH',
            colorCode: '#3d85c6'
          }, {
            name: 'Production/Programmation',
            colorCode: '#e06666'
          }, {
            name: 'Support et immeubles',
            colorCode: '#8e7cc3'
          }
      );

      var yellow = new Directory({
        name: 'Aménagement, Atelier, Logistique',
        colorCode: '#ffd966'
      });

      var green = new Directory({
        name: 'Communications/publicité',
        colorCode: '#6aa84f'
      });

      yellow.categories.push(firstCategory);
      green.categories.push(secondCategory);
      green.categories.push(thirdCategory);

      yellow.save();
      green.save();

      person1.directoryColor = yellow.colorCode;
      person2.directoryColor = green.colorCode;
      person3.directoryColor = green.colorCode;
      person4.directoryColor = green.colorCode;

      person1.directory = yellow.name;
      person2.directory = green.name;
      person3.directory = green.name;
      person4.directory = green.name;

      person1.category = firstCategory.name;
      person2.category = secondCategory.name;
      person3.category = thirdCategory.name;
      person4.category = secondCategory.name;

      person1.save();
      person2.save();
      person3.save();
      person4.save();
    });
  });
});*/