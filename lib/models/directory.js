'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Directory Schema
 */
var DirectorySchema = new Schema({
  name: String,
  colorCode: String,
  categories: [
    { type: Schema.Types.ObjectId, ref: 'Category' }
  ]
});

mongoose.model('Directory', DirectorySchema);
