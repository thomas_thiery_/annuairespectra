'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * Person Schema
 */
var PersonSchema = new Schema({
  name: String,
  surname: String,
  jobTitle: String,
  image: String,
  phone: Number,
  phoneCell: Number,
  phoneExt: Number,
  email: String,
  note: String,
  category: String,
  directory: String,
  directoryColor: String
});

mongoose.model('Person', PersonSchema);
