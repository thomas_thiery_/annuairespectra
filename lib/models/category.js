'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

/**
 * Category Schema
 */
var CategorySchema = new Schema({
    name: String,
    persons: [{ type: Schema.Types.ObjectId, ref: 'Person' }]
});

mongoose.model('Category', CategorySchema);
