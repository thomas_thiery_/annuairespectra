'use strict';

var index = require('./controllers'),
  users = require('./controllers/users'),
  persons = require('./controllers/persons'),
  directories = require('./controllers/directories'),
  categories = require('./controllers/categories'),
  session = require('./controllers/session');

var middleware = require('./middleware');

/**
 * Application routes
 */
module.exports = function (app) {

  // Server API Routes
  // app get person
  app.get('/api/persons', middleware.auth, persons.show);
  // app get all person match with the search
  app.get('/api/persons/:searchName', middleware.auth, persons.searchByName);
  // app get one person
  app.get('/api/person/:id', middleware.auth, persons.searchById);

  // app delete one person
  app.delete('/api/person/:id', middleware.auth, persons.remove);

  // app create one person
  app.post('/api/person', middleware.auth, persons.create);

  // app update one person
  app.put('/api/person/:id', middleware.auth, persons.update);

  // app get directories
  app.get('/api/directories', directories.show);

  // app get all content for a directory
  app.get('/api/directory/:directoryName/persons', middleware.auth, directories.getAllContentDirectory);

  // app delete all content associate to the directory and the directory
  app.delete('/api/directory/:id', middleware.auth, directories.remove);

  // app update a directory name and color
  app.put('/api/directory/:id', middleware.auth, directories.update);

  // app get all directories and all content
  app.get('/api/directories/persons', middleware.auth, directories.getAllContentDirectories);

  // app get all directories and all content
  app.get('/api/directories/categories', middleware.auth, directories.getAllCategoriesDirectories);

  // app create One directory
  app.post('/api/directory', middleware.auth, directories.create);

  // app get categories
  app.get('/api/categories', middleware.auth, categories.show);

  // app create One category
  app.post('/api/category', middleware.auth, categories.create);

  // app delete One category
  app.delete('/api/category/:id', middleware.auth, categories.remove);

  // app update a category
  app.put('/api/category/:id', middleware.auth, categories.update);

  //app.post('/api/users', users.create);
  app.put('/api/users', users.changePassword);
  app.get('/api/users/me', users.me);
  app.get('/api/users/:id', users.show);

  app.post('/api/session', session.login);
  app.del('/api/session', session.logout);

  // All undefined api routes should return a 404
  app.get('/api/*', function (req, res) {
    res.send(404);
  });

  // All other routes to use Angular routing in app/scripts/app.js
  app.get('/partials/*', index.partials);
  app.get('/*', middleware.setUserCookie, index.index);
};