'use strict';

var mongoose = require('mongoose'),
  Category = mongoose.model('Category'),
  Person = mongoose.model('Person'),
  Directory = mongoose.model('Directory');

/**
 * Get all categories
 */
exports.show = function (req, res) {
  return Category.find(function (err, categories) {
    if (!err) {
      return res.json(categories);
    } else {
      return res.send(err);
    }
  });
};

/**
 *  Create a category
 */
exports.create = function (req, res, next) {
  var newCategory = new Category(req.body);
  var directorySelectId = req.body.directorySelectCat;

  newCategory.save(function (err) {
    if (err) return res.json(400, err);

    return Directory.findById(directorySelectId, function (err, directory) {
      directory.categories.push(newCategory);
      directory.save(function () {
        return res.send(201);
      });
    });
  });
};

/**
 *  Update a category
 */
exports.update = function (req, res, next) {
  var id = req.params.id;
  var updateCategory = req.body;
  var updateCategoryObj = {
    name: updateCategory.name
  };

  /* Delete the old ref for category */
  Directory.update({categories: id}, {$pull: {categories: id}}, function () {
    /* find */
    Directory.findById(updateCategory.directorySelectCat, function (err, directory) {
      Category.findById(id, function (err, category) {
        Person.find({category: category.name}, function(err, persons){
          for(var i = 0; i < persons.length; i++){
            persons[i].category = updateCategory.name;
            persons[i].directory = directory.name;
            persons[i].directoryColor = directory.colorCode;
            persons[i].save();
          }
        });
      });
      Category.findByIdAndUpdate(id, updateCategoryObj, function (err, category) {
        directory.categories.push(category);
        directory.save(function () {
          return res.send(201);
        });
      });
    });
  });
};

/**
 *
 * Delete a category and his person's children
 *
 */
exports.remove = function (req, res, next) {
  var id = req.params.id;

  Category.findById(id, function (err, category) {
    if (err) return next(err);
    if (!category) return res.send(404);

    Person.find({'category': category.name})
      .remove()
      .exec(function () {
        category.remove(function () {
          res.send(204);
        });
      });
  });
};