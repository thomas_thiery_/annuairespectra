'use strict';

var mongoose = require('mongoose'),
  Directory = mongoose.model('Directory'),
  Person = mongoose.model('Person'),
  Category = mongoose.model('Category');

/**
 * Get all directories
 */
exports.show = function (req, res) {
  return Directory.find(function (err, directories) {
    if (!err) {
      return res.json(directories);
    } else {
      return res.send(err);
    }
  });
};

/**
 * Get all content for a directory
 */
exports.getAllContentDirectory = function (req, res) {
  var directoryName = req.params.directoryName;
  return Directory.findOne({'name': directoryName})
    .populate('categories')
    .exec(function (err, doc) {

      Category.find({'_id': {$in: doc.categories}})
        .populate('persons').exec(function (err, doc) {

          if (!err) {
            return res.json(doc);
          } else {
            return res.send(err);
          }
        });
    });
};

/**
 * Get all categories for all directories
 */
exports.getAllCategoriesDirectories = function (req, res) {
  var directoryName = req.params.directoryName;
  return Directory.find()
    .populate('categories')
    .exec(function (err, docs) {
      if (!err) {
        return res.json(docs);
      } else {
        return res.send(err);
      }
    });
};


/**
 * Get all content for all directories
 */
exports.getAllContentDirectories = function (req, res) {
  var directoriesExport = [];
  var count = 0;

  return Directory.find()
    .populate('categories')
    .exec(function (err, directories) {
      if (err || directories.length == 0) {
        return res.send(err);
      }

      var length = directories.length;
      directories.forEach(function (directory, index) {

        var dirTemp = {
          'name': directory.name,
          'color': directory.color,
          'colorCode': directory.colorCode,
          '_id': directory._id,
          'categories': []
        };
        directoriesExport.push(dirTemp);

        Category.find({'_id': {$in: directory.categories}})
          .populate('persons').exec(function (err, persons) {
            directoriesExport[index].categories = persons;
            count++;

            if (count === length) {
              if (!err) {
                return res.json(directoriesExport);
              } else {
                return res.send(err);
              }
            }
          });
      });
    });
};

/**
 * Remove one directory
 */
exports.remove = function (req, res, next) {
  var id = req.params.id;

  return Directory.findById(id, function (err, directory) {
    var count = 0;
    if (err || !directory) return next(err);

    Category.find({'_id': {$in: directory.categories}})
      .exec(function (err, categories) {
        if (err) return next(err);

        var length = categories.length;

        for (var i = 0; i < categories.length; i++) {
          Person.find({'category': categories[i].name}).remove().exec();
          categories[i].remove();
        }

        directory.remove(function () {
          res.json(204);
        });
      });
  });
};

/**
 *  Create a Directory
 */
exports.create = function (req, res, next) {
  var newDirectory = new Directory(req.body);
  newDirectory.save(function (err) {
    if (err) return res.json(400, err);

    return res.json(201);
  });
};

/**
 *  update a Directory
 */
exports.update = function (req, res, next) {
  var id = req.params.id;
  var updateDirectory = req.body;

  var updateDirectoryObj = {
    name: updateDirectory.name,
    color: updateDirectory.color,
    colorCode: updateDirectory.colorCode
  };

  Directory.findById(id, function (err, directory) {
    Person.update({directory: directory.name}, {directory: updateDirectory.name, directoryColor: updateDirectory.color}, function () {
      Directory.findByIdAndUpdate(id, updateDirectoryObj, function (err, directory) {
        return res.json(201);
      });
    });
  });
};