'use strict';

var mongoose = require('mongoose'),
  Person = mongoose.model('Person'),
  Category = mongoose.model('Category');

/**
 * Get all persons
 */
exports.show = function (req, res) {
  return Person.find()
    .populate('category')
    .exec(function (err, persons) {
      if (!err) {
        return res.json(persons);
      } else {
        return res.send(err);
      }
    });
};

/**
 *  Get all persons for a directory
 */
exports.directoryShow = function (req, res, next) {
  var directoryName = req.params.directoryName;

  Person.find()
    .populate('directory')
    .where('directory.name', directoryName)
    .exec(function (err, person) {
      if (err) return next(err);
      if (!person) return res.send(404);

      res.json(person);
    });
};

/**
 *  Get the person with id
 */
exports.searchById = function (req, res, next) {
  var id = req.params.id;

  Person.findById(id, function (err, person) {
    if (err) return next(err);
    if (!person) return res.send(404);

    res.json(person);
  });
};

/**
 *  Create a person
 */
exports.create = function (req, res, next) {
  var newPerson = req.body;
  var categorySelectId = req.body.categorySelect;

  /* Complete all field if empty */
  var newPersonObj = {
    category: newPerson.category ? newPerson.category : '',
    directory: newPerson.directory ? newPerson.directory : '',
    image: newPerson.image ? newPerson.image : '',
    name: newPerson.name ? newPerson.name : '',
    surname: newPerson.surname ? newPerson.surname : '',
    jobTitle: newPerson.jobTitle ? newPerson.jobTitle : '',
    phone: newPerson.phone ? newPerson.phone : '',
    phoneCell: newPerson.phoneCell ? newPerson.phoneCell : '',
    phoneExt: newPerson.phoneExt ? newPerson.phoneExt : '',
    email: newPerson.email ? newPerson.email : '',
    note: newPerson.note ? newPerson.note : '',
    directoryColor: newPerson.directoryColor ? newPerson.directoryColor : ''
  };

  newPerson = new Person(newPersonObj);

  return newPerson.save(function (err) {
    if (err) return res.json(400, err);
    /* Save the relational link to category */
    return Category.findById(categorySelectId, function (err, category) {
      if (err) return res.json(400, err);

      category.persons.push(newPerson);
      category.save(function (err) {
        if (err) return res.json(400, err);

        return res.send(201);
      });
    });
  });
};

/**
 *
 * Delete a person
 *
 */
exports.remove = function (req, res, next) {
  var id = req.params.id;

  /* old ref for person */
  Category.update({persons: id}, {$pull: {persons: id}}, function () {

    Person.findByIdAndRemove(id, function (err, person) {
      if (err) return next(err);
      if (!person) return res.send(404);

      res.send(204);
    });
  });
};

/**
 *  Modify a person with id
 */
exports.update = function (req, res, next) {
  var id = req.params.id;
  var updatePerson = req.body;
  var updatePeronObj = {
      category: updatePerson.category ? updatePerson.category : '',
      directory: updatePerson.directory ? updatePerson.directory : '',
      image: updatePerson.image ? updatePerson.image : '',
      name: updatePerson.name ? updatePerson.name : '',
      surname: updatePerson.surname ? updatePerson.surname : '',
      jobTitle: updatePerson.jobTitle ? updatePerson.jobTitle : '',
      phone: updatePerson.phone ? updatePerson.phone : '',
      phoneCell: updatePerson.phoneCell ? updatePerson.phoneCell : '',
      phoneExt: updatePerson.phoneExt ? updatePerson.phoneExt : '',
      email: updatePerson.email ? updatePerson.email : '',
      note: updatePerson.note ? updatePerson.note : '',
      directoryColor: updatePerson.directoryColor ? updatePerson.directoryColor : ''
  };

  /* Delete the old ref for person */
  Category.update({persons: id}, {$pull: {persons: id}}, function (err) {
    /* find */
    if (err) return next(err);

    Category.findOne({name: updatePerson.category}, function (err, category) {
      if (err) return next(err);

      Person.findByIdAndUpdate(id, updatePeronObj, function (err, person) {
        if (err) return next(err);

        category.persons.push(person);
        category.save(function () {
          return res.send(201);
        });
      });
    });
  });
};

/**
 *  Search some person
 */
exports.searchByName = function (req, res, next) {
  var searchName = req.params.searchName;
  var accentMap = {
    S: ['Š', 'š'],
    O: ['Œ', 'œ', 'Ò', 'Ó', 'Ò', 'Õ', 'Ö', 'Ø', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ð'],
    Z: ['Ž', 'ž'],
    y: ['Ÿ', '¥', 'Ý', 'ý', 'ÿ'],
    u: ['µ', 'Ù', 'Ú', 'Û', 'Ü', 'ù', 'ú', 'û', 'ü'],
    A: ['À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ'],
    C: ['Ç', 'ç'],
    E: ['È', 'É', 'Ê', 'Ë', 'Ẽ', 'è', 'é', 'ê', 'ë', 'ẽ'],
    I: ['Ì', 'Í', 'Î', 'Ï', 'Ĩ', 'ì', 'í', 'î', 'ï', 'ĩ'],
    D: ['Ð'],
    N: ['Ñ', 'ñ'],
    B: ['ß']};

  searchName = accentToRegex(searchName);
  searchName = searchName.replace(/\s+/g, "[-,  ]+");
  searchName = new RegExp(searchName, 'i');

  function accentToRegex(text) {
    var text = text;

    for (var index in accentMap) {
      accentMap[index].forEach(function (value) {
        /* Replace all accent*/
        text = text.replace(new RegExp(value, 'g'), index);
      });
      /* Replace all letter can be accentued by a regex*/
      text = text.replace(new RegExp(index, 'gi'), '[' + accentMap[index] + ',' + index + ']');
    }
    return text;
  }

  Person.aggregate([
    {$project: {
      fullName: {$concat: ['$name', ' ', '$surname']},
      note: 1,
      jobTitle: 1,
      name: 1,
      surname: 1,
      image: 1,
      phoneCell: 1,
      phoneExt: 1,
      directoryColor: 1
    }},
    {
      $match: { $or: [
        {fullName: searchName},
        {note: searchName},
        {jobTitle: searchName}
      ]}
    }
  ], function (err, person) {
    if (err) return next(err);
    if (!person) return res.send(404);

    res.json(person);
  });
};
