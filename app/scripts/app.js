'use strict';

angular.module('annuaireSpectraApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ngTouch'
])
  .config(function ($routeProvider, $locationProvider, $httpProvider, $compileProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'partials/directories',
        controller: 'DirectoriesCtrl',
        authenticate: true,
        reloadOnSearch: false
      })
      .when('/directory', {
        templateUrl: 'partials/directory',
        controller: 'DirectoryCtrl',
        authenticate: true
      })
      .when('/admin', {
        templateUrl: 'partials/admin',
        controller: 'adminCtrl',
        authenticate: true,
        admin: true,
        reloadOnSearch: false
      })
      .when('/login', {
        templateUrl: 'partials/login',
        controller: 'LoginCtrl'
      })
      .when('/person', {
        templateUrl: 'partials/person',
        controller: 'PersonCtrl',
        authenticate: true
      })
      .otherwise({
        redirectTo: '/'
      });

    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|file|tel|mailto|sms):/);

    $locationProvider.html5Mode(true);

    // Intercept 401s and redirect you to login
    $httpProvider.interceptors.push(['$q', '$location', function ($q, $location) {
      return {
        'responseError': function (response) {
          if (response.status === 401) {
            $location.path('/login');
            return $q.reject(response);
          }
          else {
            return $q.reject(response);
          }
        }
      };
    }]);
  })
  .run(function ($rootScope, $location, Auth) {

    $rootScope.$on('$routeChangeStart', function (event, next) {
      if (next.authenticate && !Auth.isLoggedIn()) {
        // Redirect to login if route requires auth and you're not logged in
        $location.path('/login');
      } else if (next.authenticate && next.admin && Auth.isLoggedIn() && $rootScope.currentUser.role !== 'admin') {
        // Redirect to login if route is a requires an auth admin and you're not logged in with admin route
        $location.path('/login');
      }
    });
  });