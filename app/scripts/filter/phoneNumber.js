'use strict';

/**
 * Description:
 *     Format a readable phone number with a 7 number
 *     5142221111 -> 514-222-1111
 *     http://www.noslangues-ourlanguages.gc.ca/bien-well/fra-eng/typographie-typography/telephone-eng.html
 * Usage:
 *   {{some_7number | phoneNumber}}
 */
angular.module('annuaireSpectraApp').filter('phoneNumber', function () {
  return function (tel) {
    var telString = tel.toString();
    return (!telString) ? '' : telString.slice(0,3) + '-' + telString.slice(3,6) + '-' + telString.slice(6,10);
  };
});