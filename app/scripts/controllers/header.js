'use strict';

angular.module('annuaireSpectraApp')
  .controller('headerCtrl', function ($scope, $http, $cookieStore, $filter) {
    $scope.directories = '';

    if ($cookieStore.get('Directories')) {
      $scope.directories = $cookieStore.get('Directories');
    } else {
      $http.get('/api/directories').success(function (directories) {
        directories = $filter('orderBy')(directories, 'name', false)
        $scope.directories = directories;
        $cookieStore.put('Directories', directories);
      });
    }
  });