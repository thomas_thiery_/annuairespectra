'use strict';

angular.module('annuaireSpectraApp')
  .controller('DirectoriesCtrl', function ($scope, $http) {

    $http.get('/api/directories').success(function (directories) {
      $scope.directories = directories;
    });

  });