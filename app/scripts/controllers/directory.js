'use strict';

angular.module('annuaireSpectraApp')
  .controller('DirectoryCtrl', function ($scope, $http, $location, $cookieStore) {
    var directoryName = $location.search().name.replace("/", "%2F");
    var directoryIndexNext;
    var directoryIndexPrevious;
    var directories = $cookieStore.get('Directories');
    var currentDirectoryIndex = 0;

    $scope.collapsing = function (element, e) {
      e.preventDefault();
      element.isCollapse = element.isCollapse ? false : true;
    };

    angular.forEach(directories, function (directory, key) {
      if (directory.name.replace("/", "%2F") == directoryName) {
        $scope.directory = directory;
        currentDirectoryIndex = key;
      }
    });

    $scope.next = function (e) {
      e.preventDefault();
      if (directories[currentDirectoryIndex + 1]) {
        directoryIndexNext = directories[currentDirectoryIndex + 1];
      } else {
        directoryIndexNext = directories[0];
      }
      $location.url('/directory?name=' + directoryIndexNext.name);
    };

    $scope.previous = function (e) {
      e.preventDefault();
      if (directories[currentDirectoryIndex - 1]) {
        directoryIndexPrevious = directories[currentDirectoryIndex - 1];
      } else {
        directoryIndexPrevious = directories[directories.length - 1];
      }
      $location.url('/directory?name=' + directoryIndexPrevious.name);
    };

    $http.get('api/directory/' + directoryName + '/persons').success(function (persons) {
      $scope.categories = persons;
      $scope.predicate = 'name';
    });
  });