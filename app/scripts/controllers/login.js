'use strict';

angular.module('annuaireSpectraApp')
  .controller('LoginCtrl', function ($scope, Auth, $location, $rootScope, $cookieStore) {
    $scope.user = {};
    $scope.errors = {};
    $scope.showLogin = false;

    function sendLogin(username, password){
      Auth.login({
        username: username,
        password: password
      })
        .then(function () {
          // Logged in, redirect to home
          $location.path('/');
        })
        .catch(function (err) {
          $cookieStore.remove('userPerm');
          err = err.data;
          $scope.errors.other = err.message;
        });
    }

    if($cookieStore.get('userPerm')){
      sendLogin($cookieStore.get('userPerm').password, $cookieStore.get('userPerm').password);
    } else {
      $scope.showLogin = true;
    }

    $scope.login = function (form) {
      $scope.submitted = true;
      if (form.$valid) {
        if (!$cookieStore.get('userPerm')) {
          document.cookie = 'userPerm=' + angular.toJson($scope.user) + ';path=/;max-age=' + 3888000 + ';';
        }
        // the password is used for username
        sendLogin($scope.user.password, $scope.user.password);
      }
    };
  });