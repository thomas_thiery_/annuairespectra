'use strict';

angular.module('annuaireSpectraApp')
  .controller('adminCtrl', function ($scope, $http, $cookieStore) {
    $scope.person = {};
    $scope.errors = {};
    $scope.success = {};
    $scope.submitted = {};
    $scope.predicate = 'name';
    $scope.directorySelect = null;
    $scope.directorySelectCat = null;
    $scope.categorySelect = null;
    $scope.category = null;
    $scope.personsBatch = null;
    $scope.directory = null;
    $scope.information = null;
    $scope.action = {};
    $scope.isCollapsing = {};

    /* Display */
    displayDirectory();

    $scope.collapsing = function (item, e) {
      if (!($scope.isCollapsing[item._id])) {
        $scope.isCollapsing[item._id] == 'expand';
      }
      if ($scope.isCollapsing[item._id] == 'expand') {
        $scope.isCollapsing[item._id] = 'collapse';
        item.isCollapse = false;
      } else {
        $scope.isCollapsing[item._id] = 'expand';
        item.isCollapse = true
      }
      e.preventDefault();
    };

    $scope.updateCategories = function (directory) {
      $scope.categoriesSelect = directory.categories;
      $scope.categorySelect = '';
    };

    function displayDirectory() {
      $http.get('/api/directories/persons').success(function (directories) {
        $scope.directories = directories;

        // Restore collapse and expand
        angular.forEach($scope.directories, function (directory, indexDir) {
          $scope.directories[indexDir].isCollapse = !$scope.isCollapsing[directory._id] || $scope.isCollapsing[directory._id] == 'collapse' ? false : true;

          angular.forEach(directory.categories, function (category, indexCat) {
            $scope.directories[indexDir].categories[indexCat].isCollapse = !$scope.isCollapsing[directory._id] ||  $scope.isCollapsing[category._id] == 'collapse' ? false : true;
          });
        });
        // Remove caching for directories
        $cookieStore.remove('Directories');
      }).error(function (err) {
        $scope.directories = {};
      });
    }

    function updateInformation(information) {
      $scope.information = information;
    }

    /* Modify */
    /* Modify a person */
    $scope.modifyPerson = function (person, e) {
      e.preventDefault();
      $scope.person = person;
      $scope.action = "modify";
      $('#person').css({'opacity': 1, display: 'block'});

      angular.forEach($scope.directories, function (directory) {
        if (directory.name == person.directory) {
          $scope.directorySelect = directory;
          $scope.categoriesSelect = directory.categories;

          angular.forEach(directory.categories, function (category) {
            if (category.name == person.category) {
              $scope.categorySelect = category;
            }
          });
        }
      });
    };

    /* Modify a category */
    $scope.modifyCategory = function (category, e) {
      e.preventDefault();
      $scope.category = category;
      $scope.action = "modify";
      $('#category').css({'opacity': 1, display: 'block'});

      angular.forEach($scope.directories, function (directory) {
        angular.forEach(directory.categories, function (categoryS) {
          if (category.name == categoryS.name) {
            $scope.directorySelectCat = directory;
          }
        });
      });
    };

    /* Modify a directory */
    $scope.modifyDirectory = function (directory, e) {
      e.preventDefault();
      $scope.directory = directory;
      $scope.action = "modify";
      $('#directory').css({'opacity': 1, display: 'block'});
    };

    /* Add */
    /*Add a directory */
    $scope.addDirectory = function (e) {
      e.preventDefault();
      $scope.action = "add";
      $('#directory').css({'opacity': 1, display: 'block'});
    };

    /*Add a person */
    $scope.addCategory = function (e) {
      e.preventDefault();
      $scope.action = "add";
      $('#category').css({'opacity': 1, display: 'block'});
    };

    /*Add a person */
    $scope.addPerson = function (e) {
      e.preventDefault();
      $scope.action = "add";
      $('#person').css({'opacity': 1, display: 'block'});
    };

    /*Add persons */
    $scope.addPersons = function (e) {
      e.preventDefault();
      $scope.action = "add";
      $('#personsBatch').css({'opacity': 1, display: 'block'});
    };

    /* Delete */
    /* Delete a person */
    $scope.deletePerson = function (person, e) {
      e.preventDefault();
      var deleteAnswer = confirm('Voulez-vous vraiment supprimer : ' + person.name + '?');
      if (deleteAnswer) {
        $http.delete('/api/person/' + person._id).success(function () {
          displayDirectory();
          $scope.information = "La personne: " + person.name + " a bien été supprimée";
        });
      }
    };

    /* Delete a category */
    $scope.deleteCategory = function (category, e) {
      e.preventDefault();
      var deleteAnswer = confirm('Voulez-vous vraiment supprimer : ' + category.name + '? Attention cette action va aussi supprimer toutes les fiches personnes rattachées.');
      if (deleteAnswer) {
        $http.delete('/api/category/' + category._id).success(function () {
          displayDirectory();
          $scope.information = "La catégorie: " + category.name + " a bien été supprimée";
        });
      }
    };

    /* Delete a directory */
    $scope.deleteDirectory = function (directory, e) {
      e.preventDefault();
      var deleteAnswer = confirm('Voulez-vous vraiment supprimer : ' + directory.name + '? Attention cette action va aussi supprimer toutes les fiches catégories et personnes rattachées.');
      if (deleteAnswer) {
        $http.delete('/api/directory/' + directory._id).success(function () {
          displayDirectory();
          $scope.information = "L'annuaire: " + directory.name + " a bien été supprimé";
        }).error(function () {
          $scope.directory = {};
        });
      }
    };

    /* Create or modify form */
    $scope.cancel = function () {
      $scope.category = null;
      $scope.directory = null;
      $scope.person = null;
      $scope.personsBatch = null;
      $scope.directorySelectCat = null;
      $scope.categorySelect = null;
      $scope.directorySelect = null;
      $scope.success.category = false;
      $scope.success.person = false;
      $scope.success.directory = false;
      $('.modal').css({'opacity': 0, display: 'none'});
    };

    /* Create or modify a person */
    $scope.createPerson = function (form) {
      $scope.submitted.person = true;
      if (form.$valid) {
        $scope.person.category = $scope.categorySelect.name;
        $scope.person.directory = $scope.directorySelect.name;
        $scope.person.directoryColor = $scope.directorySelect.colorCode;
        $scope.person.categorySelect = $scope.categorySelect._id;

        if ($scope.action == 'add') {
          $http.post('/api/person', $scope.person).success(function () {
            $scope.success.person = true;
            $scope.submitted.person = false;
            displayDirectory();
            $scope.cancel();
            $scope.information = "La personne: " + $scope.person.name + " a bien été ajoutée";
          }).error(function () {
            $scope.success.person = false;
          });
        } else if ($scope.action == 'modify') {
          $http.put('/api/person/' + $scope.person._id, $scope.person).success(function () {
            $scope.success.person = true;
            $scope.submitted.person = false;
            displayDirectory();
            $scope.information = "La personne: " + $scope.person.name + " a bien été modifiée";
            $scope.cancel();
          }).error(function () {
            $scope.success.person = false;
          });
        }
      }
    };

    $scope.createPersons = function (form) {
      $scope.submitted.persons = true;
      if (form.$valid) {
        var count = 0;
        var personsBatch = angular.fromJson($scope.personsBatch);
        var addAnswer = confirm('Êtes-vous certains de vouloir ajouter : ' + personsBatch.length + ' personnes?');

        if (addAnswer) {
          angular.forEach(personsBatch, function (person) {

            // Get the directory of the person
            angular.forEach($scope.directories, function (directory) {
              if (directory.name == person.directory) {

                // Get the Category of the person
                angular.forEach(directory.categories, function (category) {
                  if (category.name == person.category) {
                    person.categorySelect = category._id;
                    $http.post('/api/person', person).success(function () {
                      count++;
                      $scope.success.persons = true;
                      $scope.submitted.persons = false;
                      if (count == personsBatch.length) {
                        displayDirectory();
                        $scope.information = "Les personnes: ont bien été modifiés";
                        $scope.cancel();
                      }
                    }).error(function () {
                      $scope.success.persons = false;
                    });
                  }
                });
              }
            });
          });
        }
      }
    };

    /* Create or modify a category */
    $scope.createCategory = function (formCategory) {
      $scope.submitted.category = true;
      if (formCategory.$valid && $scope.directorySelectCat) {
        $scope.category.directorySelectCat = $scope.directorySelectCat._id;
        if ($scope.action == 'add') {
          $http.post('/api/category', $scope.category).success(function () {
            $scope.success.category = true;
            $scope.submitted.category = false;
            $scope.information = "La catégorie: " + $scope.category.name + " a bien été ajoutée";
            $scope.cancel();
            displayDirectory();

          }).error(function () {
            $scope.success.category = false;
          });
        } else if ($scope.action == 'modify') {
          $http.put('/api/category/' + $scope.category._id, $scope.category).success(function () {
            $scope.success.category = true;
            $scope.submitted.category = false;
            $scope.information = "La catégorie: " + $scope.category.name + " a bien été modifiée";
            displayDirectory();
            $scope.cancel();

          }).error(function () {
            $scope.success.category = false;
          });
        }
      }
    };

    /* Create a directory */
    $scope.createDirectory = function (formDirectory) {
      $scope.submitted.directory = true;
      if (formDirectory.$valid) {
        if ($scope.action == 'add') {
          $http.post('/api/directory', $scope.directory).success(function () {
            $scope.success.directory = true;
            $scope.submitted.directory = false;
            $scope.information = "Le répertoire: " + $scope.directory.name + " a bien été ajouté";
            displayDirectory();
            $scope.cancel();

          }).error(function () {
            $scope.success.directory = false;
          });
        }
        else if ($scope.action == 'modify') {
          $http.put('/api/directory/' + $scope.directory._id, $scope.directory).success(function () {
            $scope.success.directory = true;
            $scope.submitted.directory = false;
            $scope.information = "Le répertoire: " + $scope.directory.name + " a bien été modifié";
            displayDirectory();
            $scope.cancel();

          }).error(function () {
            $scope.success.directory = false;
          });
        }
      }
    };
  });