'use strict';

angular.module('annuaireSpectraApp')
  .controller('SearchCtrl', function ($scope, $http, $location) {
    $scope.errors = false;

    if ($location.search().search && $location.search().search != true) {
      $scope.searchWord = $location.search().search;
      searchPerson();
    }

    function searchPerson() {
      $http.get('/api/persons/' + $scope.searchWord).success(function (persons) {
        $scope.persons = persons;
        $scope.errors = false;
      });
    }

    $scope.removeSearch = function (e) {
      e.preventDefault();
      $scope.searchWord = null;
      $location.search('');
      $scope.persons = null;
    };

    $scope.search = function (formsearch) {
      $scope.submitted = true;
      if (formsearch.$valid) {
        $location.search('search', $scope.searchWord);
        searchPerson();
      } else {
        $scope.errors = true;
        $scope.persons = {};
      }
    };
  });