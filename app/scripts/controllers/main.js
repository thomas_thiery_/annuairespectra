'use strict';

angular.module('annuaireSpectraApp')
  .controller('MainCtrl', function ($scope, $http) {
    $http.get('/api/persons').success(function(persons) {
      $scope.persons = persons;
    });
  });
