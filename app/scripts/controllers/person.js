'use strict';

angular.module('annuaireSpectraApp')
  .controller('PersonCtrl', function ($scope, $http, $location, $window) {
    var personId = $location.search().id;
    $scope.search = $location.search().search;

    $scope.back = function(e){
      e.preventDefault();
      $window.history.back();
    };

    $http.get('api/person/' + personId).success(function (person) {
      $scope.person = person;
    });
  });