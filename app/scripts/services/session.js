'use strict';

angular.module('annuaireSpectraApp')
  .factory('Session', function ($resource) {
    return $resource('/api/session/');
  });
