# Annuaire Spectra is based on AngularJS + Express Full Stack Generator 
[ ![Codeship Status for thomas_thiery_/AnnuaireSpectra](https://codeship.io/projects/6067c090-e554-0131-150f-6240fcc21f63/status)](https://codeship.io/projects/25738)

Yeoman generator for creating MEAN stack applications, using MongoDB, Express, AngularJS, and Node.

Featuring: 

 * Express server integrated with grunt tasks
 * Livereload of client and server files
 * Support for Jade and CoffeeScript
 * Easy deployment workflow.
 * MongoDB integration

## Prerequisites

* MongoDB - Download and Install [MongoDB](http://www.mongodb.org/downloads) - If you plan on scaffolding your project with mongoose, you'll need mongoDB to be installed.

## Usage

Install `generator-angular-fullstack`:
```bash
npm install -g generator-angular-fullstack
```

Make a new directory, and `cd` into it:
```bash
mkdir my-new-project && cd $_
```

Run `yo angular-fullstack`, optionally passing an app name:
```bash
yo angular-fullstack [app-name]
```

## Express

Launch your express server in development mode.
```bash
grunt serve
```

Launch your express server in `debug-brk` mode with a node-inspector tab.
```bash
grunt serve:debug
``` 

Launch your express server in production mode, uses the minified/optimized production folder.
```bash
grunt serve:dist
``` 

### Livereload

`grunt serve` will watch client files in `app/`, and server files inside `lib/`, restarting the Express server when a change is detected.

## Deployment

To generate a dist folder that can easily be deployed use:

```bash
grunt
```

This will run unit tests, jshint, concatenate and minify scripts/css, compress images, add css vendor prefixes, and finally copy all files to a tidy dist folder.

Alternatively to skip tests and jshint, use:

```bash
grunt build
```

### Heroku Deployment

We provide an extremely simplifed deployment process for heroku.

`yo angular-fullstack:deploy heroku` generates a `dist` folder that is deployment ready  for [heroku.com](http://heroku.com/). 

**Create and Deploy an app in 4 steps**

1. `mkdir foo && cd foo`

2. `yo angular-fullstack`

3. `yo angular-fullstack:deploy heroku`

4. `cd dist && git push heroku master`

5. Optional (if using mongoDB) `heroku addons:add mongohq`

That's it! Your app should be live and shareable. Type `heroku open` to view it.  

## Generators

All of the **generator-angular** client side generators are available, but aliased with `angular-fullstack` to correctly generate with the fullstack folder structure. 

Angular sub-generators:

* [angular-fullstack:controller](https://github.com/yeoman/generator-angular#controller)
* [angular-fullstack:directive](https://github.com/yeoman/generator-angular#directive)
* [angular-fullstack:filter](https://github.com/yeoman/generator-angular#filter)
* [angular-fullstack:route](https://github.com/yeoman/generator-angular#route)
* [angular-fullstack:service](https://github.com/yeoman/generator-angular#service)
* [angular-fullstack:provider](https://github.com/yeoman/generator-angular#service)
* [angular-fullstack:factory](https://github.com/yeoman/generator-angular#service)
* [angular-fullstack:value](https://github.com/yeoman/generator-angular#service)
* [angular-fullstack:constant](https://github.com/yeoman/generator-angular#service)
* [angular-fullstack:decorator](https://github.com/yeoman/generator-angular#decorator)
* [angular-fullstack:view](https://github.com/yeoman/generator-angular#view)

Fullstack sub-generators:

* [angular-fullstack:deploy](#deploy)

**Note: Generators are to be run from the root directory of your app.**

Read more on the angular sub-generators from the offical [generator angular documentation][1]

  [1]: https://github.com/yeoman/generator-angular#generators